#!/bin/sh

set -e

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

path=~/omnetpp-5.6.2/projects
datapath=../data

cd $path/cpu
opp_run -l $path/queueinglib/queueinglib -u Cmdenv -c General \
--network=$1 \
--output-vector-file="\${resultdir}/$1-\${iterationvarsf}#\${repetition}.vec"

for f in $path/cpu/results/$1*.vec
do
  scavetool x $f -F JSON -o $datapath/$(basename $f).json
done