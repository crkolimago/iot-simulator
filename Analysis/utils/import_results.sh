#!/bin/sh

path=~/omnetpp-5.6.2/projects/cpu/results

for f in "$path"/*.vec
do
  scavetool x $f -F JSON -o "./data/$(basename "$f").json"
done