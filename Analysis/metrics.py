from glob import glob
import pandas as pd
import numpy as np
from scipy import stats
from scipy.stats import linregress
from itertools import repeat, zip_longest
from collections import defaultdict
import re
import matplotlib.pyplot as plt
import seaborn as sns

name_regex = re.compile(fr'\D+\d')
get_prefix = lambda r, s: re.match(r, s)[0][-1]
CREST = sns.color_palette("crest", as_cmap=True)

"""
Takes a 2D list and returns a numpy array where each row is the same length
"""
make_matrix = lambda matrix: np.array(list(zip_longest(*matrix, fillvalue=0))).T

"""
Uses make_matrix to create a numpy array
Each row is padded by the last element in that row
"""
def make_matrix_nonzero(matrix):
    none_padded_matrix = make_matrix(matrix)
    padded_matrix = np.copy(none_padded_matrix)

    for idx, l in enumerate(padded_matrix):
        if l[-1] == 0:
            l_len = len(l)
            back = l_len - 1
            while l[back] == 0:
                back -= 1
            fill_value = l[back]
            padded_matrix[idx][back+1:l_len] = fill_value
            
    return padded_matrix

"""
Takes 2 dense arrays, representing a series of timestamps and the data at each one
and makes them sparse, duplicating the value for each second within a timestamp range
"""
def make_sparse_array(times, values):
    sparse = []
    for i in range(len(times)-1):
        j = i+1

        span = times[j] - times[i]
        value = values[i]

        sparse += [value]*span
    return sparse

def gather_metrics(runs):
    std_queue_lengths = []
    std_system_loads = []
    sum_system_power = []
    
    for alg in runs:
        systems = runs[alg]
        
        system_queue_lengths = []
        system_server_loads = defaultdict(list)
        system_power_totals = defaultdict(list)
        
        for key in systems:
            system = systems[key]

            for module in system:
                statistic_name = module['name'].split(':')[0]
                module_name = module['module'].split('.')[1]
                

                times = module['time']
                values = module['value']
                sparse_array = make_sparse_array(times, values)

                if statistic_name == 'queueLength':
                    system_queue_lengths.append(sparse_array)
                elif statistic_name == 'busy':
                    system_server_loads[key].append(sparse_array)
                elif statistic_name == 'powerConsumed':
                    cumulative_values = np.cumsum(module['value'])
                    sparse_array = make_sparse_array(module['time'], cumulative_values)
                    system_power_totals[key].append(sparse_array)
                    
        m = make_matrix(system_queue_lengths)
        t = np.std(m, axis=0)
        std_queue_lengths.append(t)
        
        md = list(system_server_loads.values())
        mdc = [np.mean(make_matrix(x), axis=0) for x in md]
        m = make_matrix(mdc)
        t = np.std(m, axis=0)
        std_system_loads.append(t)

        md = list(system_power_totals.values())
        mdc = [np.sum(make_matrix(x), axis=0) for x in md]
        m = make_matrix(mdc)
        t = np.sum(m, axis=0)
        sum_system_power.append(t)

    mm = make_matrix(std_queue_lengths)
    df = pd.DataFrame(mm.T, columns=list(runs))
    sns.boxplot(data=df)
    
    plt.title('Standard Deviation of Queue Lengths')
    plt.xlabel('Algorithm')
    plt.ylabel('Standard Deviation')
    plt.show()
    
    mm = make_matrix(std_system_loads)
    df = pd.DataFrame(mm)
    sns.heatmap(df, cmap=CREST)
    
    plt.title('Standard Deviation of System Loads')
    plt.xlabel('Algorithm')
    plt.ylabel('Standard Deviation')
    plt.show()

    mm = make_matrix(std_system_loads)
    df = pd.DataFrame(mm)
    sns.heatmap(df, cmap=CREST)
    
    plt.title('Standard Deviation of System Loads')
    plt.xlabel('Algorithm')
    plt.ylabel('Standard Deviation')
    plt.show()
    
    mm = make_matrix(sum_system_power)
    df = pd.DataFrame(mm.T, columns=list(runs))
    sns.lineplot(data=df)
    sns.pointplot(data=df)
    sns.boxplot(data=df)
    sns.lmplot(data=df)
    
    plt.title('Rate of Power Consumption')
    plt.xlabel('Simulation Time (s)')
    plt.ylabel('Power Consumed (mW)')
    plt.show()

    for a in sum_system_power:
      x = list(range(len(a)))
      res = linregress(x, a)
      y = [res.intercept + n*res.slope for n in x]
      plt.plot(x, y)
    
    plt.title('Slope of Rate of Power Consumption')
    plt.xlabel('Simulation Time (s)')
    plt.ylabel('Power Consumed (mW)')
    plt.show()

files = glob('data/*')

file_groups = defaultdict(list)

for filename in files:
    prefix = filename.split('/')[1].split('-')[0]
    file_groups[prefix].append(filename)

for group in file_groups:
    file_group = file_groups[group]
    print(group)

    runs = {}
    for filename in file_group:
        results = eval(open(filename).read())
        
        for key in results:
            result = results[key]
            
            attributes = result['attributes']
            algorithm = attributes["iterationvarsf"].replace("-", "")
            run_name = f'{attributes["configname"]}-{algorithm}'
            config_name = attributes['configname']
            vectors = result['vectors']
            
            systems = defaultdict(list)
            for vector in vectors:
                submodule = vector['module'].split('.')[-1] ####
                prefix = get_prefix(name_regex, submodule)
                systems[int(prefix)].append(vector)

            runs[algorithm] = systems

    gather_metrics(runs)

    print()