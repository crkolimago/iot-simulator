import org.java_websocket.WebSocket;

public class ClientInfo {
    private int capacity;
    private float overload;
    private WebSocket websocket;

    public ClientInfo(int capacity, float overload, WebSocket websocket) {
        this.capacity = capacity;
        this.overload = overload;
        this.websocket = websocket;
    }

    public int getCapacity() {
        return capacity;
    }

    public float getOverload() {
        return overload;
    }

    public WebSocket getWebsocket() {
        return websocket;
    }

    @Override
    public String toString() {
        return "[c,o](" + capacity + "," + overload + ")";
    }
}