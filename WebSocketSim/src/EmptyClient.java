import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;


public class EmptyClient extends WebSocketClient {
    private ClientState state;
    private Logger logger;

    public EmptyClient(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public EmptyClient(URI serverURI, Logger logger, ClientState state) {
        super(serverURI);
        this.logger = logger;
        this.state = state;
    }

    @Override
    public void onOpen(ServerHandshake handshakeData) {
        logger.info("Sending INIT message");
        send(String.format("INIT - %d,%f", state.getCapacity(), state.getOverload()));
        logger.info("Sending READY message");
        send("READY - null");
        logger.info("new connection opened");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        logger.info("closed with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(String message) {
        handleStringMessage(message);
    }

    protected void handleStringMessage(String message) {
        String[] messageParts = message.split("-");

        if (messageParts.length < 2) {
            logger.error("Improperly formatted message: " + message);
            return;
        }

        String messageType = messageParts[0].trim();
        String messageBody = message.substring(message.indexOf('-') + 1).trim();

        switch (messageType) {
            case "TASK":
                logger.info("Processing TASK message from server with body: " + messageBody);

                int work;
                try {
                    work = Integer.parseInt(messageBody);
                } catch(Exception e) {
                    logger.error("Error parsing server task");
                    break;
                }

                // TODO: do some work now!!!
                logger.info("Doing " + work + " units of work!");

                logger.info("Sending READY message");
                send("READY - null");

                break;
            default:
                logger.warn("Unrecognized message type");
                break;
        }
    }

    @Override
    public void onMessage(ByteBuffer message) {
        logger.info("received ByteBuffer");
    }

    @Override
    public void onError(Exception ex) {
        logger.error("an error occurred:" + ex);
    }

    public static void main(String[] args) throws URISyntaxException {
        if (args.length < 3) {
            System.out.println("Usage: java -jar EmptyClient <serverIp> <capacity> <overload>");
            System.exit(-1);
        }

        String serverIp = args[0];
        int capacity = -1;
        float overload = -1;
        try {
            capacity = Integer.parseInt(args[1]);
            overload = Float.parseFloat(args[2]);
        } catch (Exception ex) {
            System.err.println("Error parsing command line arguments: " + ex);
            System.exit(-1);
        }

        if (capacity <= 0 || overload <= 0) {
            System.err.println("Invalid command line arguments");
            System.exit(-1);
        }
        
        Logger logger = LoggerFactory.getLogger(EmptyClient.class);
        logger.debug("Main starting");

        ClientState state = new ClientState(logger, capacity, overload);

        WebSocketClient client = new EmptyClient(new URI("ws://" + serverIp + ":8887"), logger, state);

        boolean isOnline = false;

        for (int i = 1; !isOnline && i <= 3; i++) {
            try {
                logger.debug("Attempting to connect to " + serverIp);
                Socket clientSocket = new Socket(serverIp, 8887);
                clientSocket.getInetAddress().isReachable(10000);
                logger.info("Server is online");
                isOnline = true;
            } catch (ConnectException ex) {
                logger.debug("Server is offline; Trying again in " + i + " seconds");

                try {
                    Thread.sleep(i * 1000);
                } catch (InterruptedException iex) {
                    logger.error(iex.toString());
                }
            } catch (Exception ex) {
                logger.error(ex.toString());
            }
        }

        if (!isOnline) {
            logger.error("Unable to connect to the server");
            System.exit(-1);
        }

        client.connect();
    }
}
