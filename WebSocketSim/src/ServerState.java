import org.slf4j.Logger;

import java.util.*;

public class ServerState {
    private Queue<String> queue;
    private Map<String, ClientInfo> map;
    private Logger logger;
    private String algorithm = "RR";
    public boolean waiting = true;
    private int expectedClients;
    private int workLeft;

    public ServerState(Logger logger, int expectedClients, int totalWork) {
        queue = new LinkedList<>();
        map = new HashMap<>();
        this.logger = logger;
        this.expectedClients = expectedClients;
        this.workLeft = totalWork;

        logger.debug("New server state created with " + expectedClients + " clients and " + totalWork + " work units.");
    }

    /**
     * If map does not contain key, an error is logged
     * Otherwise, the map value is updated and the key is added to the queue
     * @param key
     * @param value
     */
    public void add(String key, ClientInfo value) {
        if (map.containsKey(key)) {
            logger.warn("[" + key + "] is already initialized");
            return;
        }

        logger.debug("Adding [" + key + "] to queue; initializing with values: " + value);
        queue.add(key);
        map.put(key, value);
    }

    public ClientInfo get(String key) {
        if (!map.containsKey(key)) {
            logger.error("[" + key + "] does not exist");
            return null;
        }
        return map.get(key);
    }

    public void remove(String key) {
        if (!map.containsKey(key)) {
            logger.error("[" + key + "] does not exist");
            return;
        }

        map.remove(key);

        try {
            queue.remove(key);
        } catch (NoSuchElementException ex) {
            // suppress error
        }
    }

    public int clientsJoined() { return map.size(); }

    public int clientsInQueue() {
        return queue.size();
    }

    public String algorithm() {
        return algorithm;
    }

    public int getExpectedClients() {
        return expectedClients;
    }

    public int getWorkLeft() {
        return workLeft;
    }

    public void subtractWork(int done) {
        this.workLeft = this.workLeft - done;
    }

    public String getNext() {
        return queue.remove();
    }
}