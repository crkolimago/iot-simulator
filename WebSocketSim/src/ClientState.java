import org.slf4j.Logger;

public class ClientState {
    private int capacity;
    private float overload;

    public ClientState(Logger logger, int capacity, float overload) {
        this.capacity = capacity;
        this.overload = overload;
    }

    public int getCapacity() {
        return capacity;
    }

    public float getOverload() {
        return overload;
    }
}
