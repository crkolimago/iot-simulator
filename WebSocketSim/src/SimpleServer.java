import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class SimpleServer extends WebSocketServer {
    private ServerState state;
    private Logger logger;

    public SimpleServer(InetSocketAddress address, Logger logger, ServerState state) {
        super(address);
        this.logger = logger;
        this.state = state;
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        logger.info("new connection to " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        logger.info("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
        state.remove(getKey(conn));
        logger.debug(state.clientsJoined() + " clients connected");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        handleStringMessage(getKey(conn), message, conn);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message ) {
        logger.info("received ByteBuffer from "	+ conn.getRemoteSocketAddress());
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        logger.error("an error occurred on connection " + conn.getRemoteSocketAddress()  + ":" + ex);
    }

    @Override
    public void onStart() {
        logger.info("server started successfully");

        Timer timer = new Timer();
        TimerTask exitApp = new TimerTask() {
            public void run() {
                if (state.clientsInQueue() < state.getExpectedClients()) {
                    logger.error("Not enough clients joined. Exiting...");
                    System.exit(-1);
                }
            }
        };

        timer.schedule(exitApp, new Date(System.currentTimeMillis()+8*1000));
    }

    private String getKey(WebSocket conn) {
        return conn.getRemoteSocketAddress().toString();
    }

    protected void handleStringMessage(String key, String message, WebSocket websocket) {
        String[] messageParts = message.split("-");

        if (messageParts.length < 2) {
            logger.error("Improperly formatted message: " + message);
            return;
        }

        String messageType = messageParts[0].trim();
        String messageBody = message.substring(message.indexOf('-') + 1).trim();

        ClientInfo info;

        switch (messageType) {
            /*
             * Contains comma-separated ClientInfo details in order
             * Responsible for queueing up clients until the intended number is reached
             */
            case "INIT":
                logger.info("Processing INIT message from client [" + key + "]  with body: " + messageBody);
                String[] clientInfoParts = messageBody.split(",");
                if (clientInfoParts.length < 2) {
                    logger.error("Improperly formatted body");
                    break;
                }

                int capacity;
                float overload;
                try {
                    capacity = Integer.parseInt(clientInfoParts[0]);
                    overload = Float.parseFloat(clientInfoParts[1]);
                } catch(Exception e) {
                    logger.error("Error parsing client info");
                    break;
                }

                info = new ClientInfo(capacity, overload, websocket);
                logger.info("Client info: " + info);

                state.add(key, info);

                break;
            /*
             *  Signals that a client is waiting
             *  Will not dispatch work until the number of intended clients have joined
             */
            case "READY":
                logger.info("Processing READY message from client [" + key + "] with body: " + messageBody);

                if (state.getWorkLeft() <= 0) {
                    logger.debug("No more work! Exiting...");
                    System.exit(0);
                }

                if (state.waiting) {  // waiting for all clients to come in
                    logger.debug("Waiting for " + (state.getExpectedClients() - state.clientsInQueue()) + " clients to join...");
                    if (state.clientsInQueue() >= state.getExpectedClients()) {
                        logger.debug("Expected number of clients joined");
                        switch (state.algorithm()) {
                            default:
                                logger.error("Algorithm not implemented, defaulting to RR");
                            case "RR":
                                while (state.clientsInQueue() > 0) {
                                    String next = state.getNext();
                                    info = state.get(next);
                                    sendWork(info);
                                }
                                break;
                        }
                        state.waiting = false;
                    }
                } else {
                    info = state.get(key);
                    sendWork(info);
                }
                break;
            default:
                logger.warn("Unrecognized message type");
                break;
        }
    }

    private void sendWork(ClientInfo info) {
        WebSocket conn = info.getWebsocket();

        int work = getClientWork(info);
        state.subtractWork(work);

        logger.info("Sending TASK message to " + getKey(conn));
        conn.send("TASK - " + work);
    }

    private int getClientWork(ClientInfo info) {
        int clientCapacity = info.getCapacity();
        float clientOverflow = info.getOverload();
        int workLeft = state.getWorkLeft();

        int calculatedWork = Math.round(clientCapacity * clientOverflow);

        logger.debug(String.format("capacity: %d, overload: %f, remainder: %d, calculated: %d",
                clientCapacity, clientOverflow, workLeft, calculatedWork));

        return Math.min(calculatedWork, workLeft);
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: java -jar SimpleServer <expectedClients> <totalWork>");
            System.exit(-1);
        }

        int port = 8887;

        int expectedClients = -1, totalWork = -1;
        try {
            expectedClients = Integer.parseInt(args[0]);
            totalWork = Integer.parseInt(args[1]);
        } catch (Exception ex) {
            System.err.println("Error parsing command line arguments " + ex);
            System.exit(-1);
        }

        if (expectedClients <= 0 || totalWork <= 0) {
            System.err.println("Invalid command line arguments");
            System.exit(-1);
        }

        Logger logger = LoggerFactory.getLogger(SimpleServer.class);
        ServerState state = new ServerState(logger, expectedClients, totalWork);

        WebSocketServer server = new SimpleServer(new InetSocketAddress(port), logger, state);
        server.run();
    }
}
