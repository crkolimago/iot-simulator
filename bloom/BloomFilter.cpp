#include <iostream>
#include <cstdint>
#include <array>
#include <climits>
#include "BloomFilter.h"
#include "MurmurHash3.h"

BloomFilter::BloomFilter(uint64_t size, uint8_t numHashes)
      : m_bits(size, 0),
        m_numHashes(numHashes) {}

std::array<uint64_t, 2> hash(const uint8_t *data,
                             std::size_t len) {
  std::array<uint64_t, 2> hashValue;
  MurmurHash3_x64_128(data, len, 0, hashValue.data());

  return hashValue;
}

void BloomFilter::print() {
  for (uint8_t i: m_bits) {
    std::cout << unsigned(i) << ' ';
  }
  std::cout << std::endl;
}

inline uint64_t nthHash(uint8_t n,
                        uint64_t hashA,
                        uint64_t hashB,
                        uint64_t filterSize) {
    return (hashA + n * hashB) % filterSize;
}

void BloomFilter::add(const uint8_t *data, std::size_t len) {
  auto hashValues = hash(data, len);

  for (int n = 0; n < m_numHashes; n++) {
      m_bits[nthHash(n, hashValues[0], hashValues[1], m_bits.size())]++;
  }
}

int BloomFilter::possiblyContains(const uint8_t *data, std::size_t len) const {
  auto hashValues = hash(data, len);
  int min = INT_MAX;

  for (int n = 0; n < m_numHashes; n++) {
      int value = m_bits[nthHash(n, hashValues[0], hashValues[1], m_bits.size())];
      if (value == 0) {
          return 0;
      } else {
        if (value < min) {
          min = value;
        }
      }
  }

  return min;
}