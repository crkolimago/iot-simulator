#include <iostream>
#include "BloomFilter.h"
#include <cassert>

#define SIZE 10

void add(BloomFilter *bf, std::string s)
{
  std::cout << "adding: " << s << std::endl;
  bf->add((uint8_t*)s.c_str(), s.size());
  bf->print();
}

void exists(BloomFilter bf, std::string s)
{
  int possiblyContains = bf.possiblyContains((uint8_t*)s.c_str(), s.size());
  if (possiblyContains) {
    std::cout << "Possibly contains: " << s << " " << possiblyContains << " times\n";
  } else {
    std::cout << "Definitely does not contain: " << s << std::endl;
  }
}

int main()
{
  BloomFilter bf(10, 2);

  bf.print();

  std::string abc = "abc";

  add(&bf, abc);
  add(&bf, abc);
  add(&bf, abc);

  exists(bf, abc);

  std::string def = "def";

  add(&bf, def);
  add(&bf, def);
  add(&bf, def);

  exists(bf, def);

  bf.print();

  exists(bf, abc);
  exists(bf, def);
  exists(bf, "ghi");
  exists(bf, "jkl");
  exists(bf, "mno");
}