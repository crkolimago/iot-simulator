import sys, json

if len(sys.argv) < 2:
  print('usage:', sys.argv[0], 'filename.json name')
  exit()

### ini generation ###

param_defs = {
  'interArrivalTime': 'source',
  'numJobs': 'source',
  'serviceTime': 'server',
  'capacity': 'queue',
  'jobPriority': 'source',
  'jobCycles': 'source',
  'jobPowerConsumption': 'source',
}

infile = sys.argv[1]
with open(infile, 'r') as f:
  network_definition = json.load(f)

name = sys.argv[2]

params = ""
for i in range(len(network_definition)):
  current = network_definition[i]
  for k in current.keys():
    if k in param_defs.keys():
      node = param_defs[k]
      params += f"**.{node}{i}.{k} = {current[k]}\n"
  params += "\n"

ned = f"""network = {name}

{params}"""

print(ned)

### ned generation ###

ned_submodules = ""
ned_connections = ""

num_nodes = len(network_definition)

for n in range(num_nodes):
  ned_submodules += f"""    source{n}: Source;
    queue{n}: PassiveQueue;

"""
  ned_connections += f"""    source{n}.out --> queue{n}.in++;
    queue{n}.out++ --> server.in++;

"""

ned_submodules += "    server: Server;\n    sink: Sink;\n\n"
ned_connections += "    server.out --> sink.in++;\n"

# for i in range(num_nodes):
#   for j in range(num_nodes):
#     if i != j:
#       ned_connections += f"    queue{i}.out++ --> server{j}.in++;\n"

ned = f"""import org.omnetpp.queueing.Source;
import org.omnetpp.queueing.PassiveQueue;
import org.omnetpp.queueing.Server;
import org.omnetpp.queueing.Sink;

network {name}
{{
  submodules:
{ned_submodules}  connections:
{ned_connections}}}"""

print(ned)