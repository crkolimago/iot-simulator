import sys

if len(sys.argv) < 2:
  print('usage:', sys.argv[0], 'len name')
  exit()

ned_submodules = ""
ned_connections = ""

num_nodes = int(sys.argv[1])
name = sys.argv[2]

for n in range(num_nodes):
  ned_submodules += f"""    source{n}: Source;
    queue{n}: PassiveQueue;
    server{n}: Server;
    sink{n}: Sink;

"""
  ned_connections += f"""    source{n}.out --> queue{n}.in++;
    queue{n}.out++ --> server{n}.in++;
    server{n}.out --> sink{n}.in++;

"""

for i in range(num_nodes):
  for j in range(num_nodes):
    if i != j:
      ned_connections += f"    queue{i}.out++ --> server{j}.in++;\n"

ned = f"""import org.omnetpp.queueing.Source;
import org.omnetpp.queueing.PassiveQueue;
import org.omnetpp.queueing.Server;
import org.omnetpp.queueing.Sink;

network {name}
{{
  submodules:
{ned_submodules}  connections:
{ned_connections}}}"""

print(ned)
