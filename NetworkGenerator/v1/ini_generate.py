import sys, json

if len(sys.argv) < 2:
  print('usage:', sys.argv[0], 'filename.json name')
  exit()

param_defs = {
  'interArrivalTime': 'source',
  'numJobs': 'source',
  'serviceTime': 'server',
  'capacity': 'queue',
  'jobPriority': 'source',
  'jobCycles': 'source',
  'jobPowerConsumption': 'source',
}

infile = sys.argv[1]
with open(infile, 'r') as f:
  network_definition = json.load(f)

name = sys.argv[2]

params = ""
for i in range(len(network_definition)):
  current = network_definition[i]
  for k in current.keys():
    if k in param_defs.keys():
      node = param_defs[k]
      params += f"**.{node}{i}.{k} = {current[k]}\n"
  params += "\n"

ned = f"""network = {name}

{params}"""

print(ned)