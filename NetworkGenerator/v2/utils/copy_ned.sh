#!/bin/sh

# used to share files between WSL instances (e.g. Ubuntu 20 for python and Ubuntu 18 for OMNeT++)
mkdir /mnt/wsl/share
cp $1 /mnt/wsl/share/$1