import argparse

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-n", "--name", default="Fog", help="name for the generated network")
  parser.add_argument("-d", "--disconnected", help="disable full interconnectivity", action="store_true")
  parser.add_argument("items", nargs="+", help="space separated list of resource lengths for each node")

  args = parser.parse_args()

  return vars(args)