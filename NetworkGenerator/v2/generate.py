import args

def main(name, nodes, disconnected=False):
  n_resources = [int(a) for a in nodes]
  n_systems = len(n_resources)

  imports = """import org.omnetpp.queueing.Source;
import org.omnetpp.queueing.PassiveQueue;
import org.omnetpp.queueing.Server;
import org.omnetpp.queueing.Sink;"""

  assert(n_systems == len(n_resources))

  submodules = ""
  connections = ""

  for i in range(n_systems):
    submodule = f"\t\tsource{i}: Source;\n\t\tsink{i}: Sink;\n\t\tqueue{i}: PassiveQueue {{\n"
    submodule += f"\t\t\tselfServers = {n_resources[i]};\n\t\t}};\n"
    connection = f"\t\tsource{i}.out --> queue{i}.in++;\n"

    for j in range(n_resources[i]):
      submodule += f"\t\tserver{i}{j}: Server;\n"
      connection += f"\t\tserver{i}{j}.out --> sink{i}.in++;\n\t\tqueue{i}.out++ --> server{i}{j}.in++;\n"

    submodules += f"{submodule}\n"
    connections += f"{connection}\n"

  additional = ""
  if not disconnected:
    for i in range(n_systems):
      for j in range(n_systems):
        if i != j:
          for k in range(n_resources[j]):
            additional += f"\t\tqueue{i}.out++ --> server{j}{k}.in++;\n"
      additional += "\n"

  ned = f"{imports}\n\nnetwork {name}\n{{\n\tsubmodules:\n{submodules}\tconnections:\n{connections}{additional}}}"

  print(ned)
  with open(f'{name}.ned', 'w') as f:
    f.write(ned)

if __name__ == '__main__':
  arguments = args.main()

  main(arguments['name'], arguments['items'], arguments['disconnected'])