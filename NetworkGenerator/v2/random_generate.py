import generate
import random
import args

random.seed(0)

nodes = [random.randint(1,3) for _ in range(5)]

arguments = args.main()

generate.main(arguments['name'], nodes, arguments['disconnected'])