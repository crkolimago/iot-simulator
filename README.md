# Iot Simulator #

*Websockets load sharing server/client simulator and instructions for use within COSSIM framework*

### Setup ###

###### WebSocketSim Quick Start ######

1. Open WebSocketSim project in IntelliJ
2. Build project
3. Test functionality locally by running Application configurations (Server and Client)
    - Edit configuration to change application arguments
        - SimpleServer requires the number of clients expected to join and the number of work units
        - EmptyClient requires the IP of the server, the amount of work the client can perform, and the amount of work the client should perform relative to the total
    - Server and Client are individually designed to time out if no connection is made within a set time period
4. Packaged JAR files are produced in build in the [artifacts](WebSocketSim/out/artifacts/) directory

###### COSSIM Quick Start ######

**Note:** This quick start guide is for a simple two node example; Much of the following advice is a condensed version of the [COSSIM Framework User Guide](#cossim-guide) with annotations; Refer to User Guide for additional information or debugging

1. Download Ubuntu 14.04 VM at [COSSIM Framework Repo](#resources)
    - Requires VMWare Workspace
    - Alternatively follow setup instruction in [COSSIM Framework User Guide](#resources)
2. From unzipped directory, double click on `COSSIM_Integration_with_Real_OMNET_VM2.ovf` to open in VMWare
3. Sign into VM with password `cossim1234`
4. Open a terminal and enter the command `rtig` (leave running)
5. Open a second terminal and enter the command `SynchServer` (leave running)
6. Open a third terminal and enter the command `omnetpp` to launch the Omnet++ GUI
7. Select the COSSIM configuration button to begin config
8. Specify 2 nodes, 1 cluster, and 10ms synchronization time
9. Select ARM-32, Etherdump, Script, and Power
    - Etherdump = metrics and statistics dump
    - Script = automatically execute configuration script in gem5 after creation
    - Power = power specific metrics dump
10. Configure nodes with defaults
    - disk-image: linux-aarch32-ael.img
    - mem-size: 512
    - cores: 1
    - RxPacketTime: 10 ms
11. Select **Done** in the review page
12. Replace the contents of ARPTest.ned source with the SimpleARPTest.ned file contents
13. In the Omnet++ solution explorer, select `test/simulations/omnetpp.ini`
14. Click the green Run button to enter the simulation page
15. Select OK.
16. Navigate to the file explorer
17. Open `~/COSSIM/gem5/configs/boot/COSSIM` and modify the following lines
    - In `script0.rcS`
        - set the eth0 to `10.0.1.2` and the default gateway as `10.0.1.1`
        - replace the ping command with `java -jar SimpleServer.jar 1 10`
    - In `script1.rcS`
        - set the eth0 to `10.0.1.3` and the default gateway as `10.0.1.1`
        - replace the ping command with `java -jar EmptyClient.jar 10.0.1.2 10 0.4`
18. Copy the Java JAR files to the Desktop of the VM
19. Open a terminal and navigate to `~/Desktop`
20. Execute `./mount.sh ARM32 NAME-OF-JAR` to move the JAR files to the ARM32 image root
    - To be consistent with step 17, the name of the JAR files should be `SimpleServer.jar` and `EmptyClient.jar`
21. Back in the Omnet++ simulation page select the red arrow to Run
22. Wait for simulation to finish
    - To monitor execution messages: in a new terminal enter `m5term 127.0.0.1 PORT` where *PORT* starts at 3000 and is incremented by 1 for each successive node
23. Back in the Omnet IDE, select COSSIM Results to view the results for each node

### Resources ###

* COSSIM
    * [COSSIM Paper](https://dl.acm.org/doi/fullHtml/10.1145/3378934)
    * [COSSIM Framework Repo](https://github.com/H2020-COSSIM/COSSIM_framework)
    * [User Guide](https://github.com/H2020-COSSIM/COSSIM_framework/blob/master/COSSIMUG_V1.1.pdf)
    * [gem5 Documentation](https://www.gem5.org/documentation/general_docs/m5ops/)

* OMNeT++
    * [User Guide](https://doc.omnetpp.org/omnetpp/UserGuide.pdf)
    * [Tictoc Tutorial](https://docs.omnetpp.org/tutorials/tictoc/)
    * [Porting Protocols](https://docs.omnetpp.org/articles/porting-code-into-omnetpp/)
    * [Simulation Models and Tools](https://omnetpp.org/download/models-and-tools.html)
    * [Developer's Guide](https://doc.omnetpp.org/omnetpp4/ide-developersguide/ide-developersguide.html)

* Java
    * [WebSockets Library](https://github.com/TooTallNate/Java-WebSocket)
    * [CPU Load Generator](https://caffinc.github.io/2016/03/cpu-load-generator/)

### Debugging ###

* COSSIM
    * Unable to download COSSIM VM files: too large
        * Download each file individually and store in a directory
    * Simulation is stuck in AEL login state
        * Read from script not selected (Figure 7 in User Guide)
    * Error occurs in Omnet simultion page, after loading but before execution
        * Full reset rtig and/or SynchServer terminals (i.e. new terminal)

### OMNeT++ ###

[scavetool](https://doc.omnetpp.org/omnetpp/manual/#sec:ana-sim:scavetool)
- To view information on the command line: `scavetool -l <filename>.{sca,vec}`

* I strongly recommend that you setup OMNeT++ in Windows Subsystem for Linux (WSL2)
  * [Docs](https://docs.omnetpp.org/articles/wsl2/) - A really good website with technical articles as well as a tutorial that will get you up to speed with building modules and networks