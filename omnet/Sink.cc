//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2006-2015 OpenSim Ltd.
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "Sink.h"
#include "Job.h"
#include "Server.h"

namespace queueing {

Define_Module(Sink);

void Sink::initialize()
{
    lifeTimeSignal = registerSignal("lifeTime");
    totalQueueingTimeSignal = registerSignal("totalQueueingTime");
    queuesVisitedSignal = registerSignal("queuesVisited");
    totalServiceTimeSignal = registerSignal("totalServiceTime");
    totalDelayTimeSignal = registerSignal("totalDelayTime");
    delaysVisitedSignal = registerSignal("delaysVisited");
    generationSignal = registerSignal("generation");
    importanceSignal = registerSignal("importance");
    cyclesSignal = registerSignal("cycles");
    powerConsumptionSignal = registerSignal("powerConsumption");
    keepJobs = par("keepJobs");
}

void Sink::handleMessage(cMessage *msg)
{
    Job *job = check_and_cast<Job *>(msg);

    EV_DEBUG << getName() << " received " << job->getName() << endl;

    // gather statistics
    emit(lifeTimeSignal, simTime()- job->getCreationTime());
    emit(totalQueueingTimeSignal, job->getTotalQueueingTime());
    emit(queuesVisitedSignal, job->getQueueCount());
    emit(totalServiceTimeSignal, job->getTotalServiceTime());
    emit(totalDelayTimeSignal, job->getTotalDelayTime());
    emit(delaysVisitedSignal, job->getDelayCount());
    emit(generationSignal, job->getGeneration());

    emit(importanceSignal, job->getImportance());
    emit(cyclesSignal, job->getCycles());
    emit(powerConsumptionSignal, job->getPowerConsumption());

    int gateSize = this->gateSize("in");
    for (int i=0; i<gateSize; i++) {
        cModule *module = this->gate("in", i)->getPreviousGate()->getOwnerModule();
        IServer *server = dynamic_cast<IServer *>(module);
        EV_INFO << getName() << " checking if " << module->getName() << " is idle: " << server->isIdle() << endl;
    }

    if (!keepJobs)
        delete msg;
}

void Sink::finish()
{
    // TODO missing scalar statistics
}

}; //namespace

