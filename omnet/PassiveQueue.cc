//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2006-2015 OpenSim Ltd.
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "PassiveQueue.h"
#include "Job.h"
#include "IServer.h"

namespace queueing {

Define_Module(PassiveQueue);

PassiveQueue::PassiveQueue()
{
    selectionStrategy = nullptr;
}

PassiveQueue::~PassiveQueue()
{
    delete selectionStrategy;
}

void PassiveQueue::initialize()
{
    droppedSignal = registerSignal("dropped");
    queueingTimeSignal = registerSignal("queueingTime");
    queueLengthSignal = registerSignal("queueLength");
    emit(queueLengthSignal, 0);

    selfServers = par("selfServers");
    capacity = par("capacity");
    queue.setName("queue");

    fifo = par("fifo");

    selectionStrategy = SelectionStrategy::create(par("sendingAlgorithm"), this, false);
    if (!selectionStrategy)
        throw cRuntimeError("invalid selection strategy");
}

void PassiveQueue::handleMessage(cMessage *msg)
{
    Job *job = check_and_cast<Job *>(msg);
    job->setTimestamp();

    // check for container capacity
    if (capacity >= 0 && queue.getLength() >= capacity) {
        EV_INFO << getName() << " full! Job dropped.\n";
        if (hasGUI())
            bubble("Dropped!");
        emit(droppedSignal, 1);
        delete msg;
        return;
    }

    int k = selectionStrategy->select();
    if (k < 0 || k > selfServers) {
        EV_INFO << getName() << " did not find an empty server for " << job->getName() << endl;
        // enqueue if no idle server found
        queue.insert(job);
        emit(queueLengthSignal, length());
        job->setQueueCount(job->getQueueCount() + 1);
    }
    else if (length() == 0) {
        cGate *gate = selectionStrategy->selectableGate(k);
        EV_INFO << getName() << " sending job to " << gate->getOwnerModule()->getName() << endl;
        // send through without queueing
        sendJob(job, k);
    }
    else
        throw cRuntimeError("This should not happen. Queue is NOT empty and there is an IDLE server attached to us.");
}

void PassiveQueue::refreshDisplay() const
{
    // change the icon color
    getDisplayString().setTagArg("i", 1, queue.isEmpty() ? "" : "cyan");
}

Job *PassiveQueue::peek()
{
    return (Job *)queue.front();
}

int PassiveQueue::length()
{
    return queue.getLength();
}

void PassiveQueue::request(int gateIndex)
{
    Enter_Method("request()!");
    EV_INFO << getName() << " received a job request\n";

    ASSERT(!queue.isEmpty());

    Job *job;
    if (fifo) {
        job = (Job *)queue.pop();
    }
    else {
        job = (Job *)queue.back();
        // FIXME this may have bad performance as remove uses linear search
        queue.remove(job);
    }
    emit(queueLengthSignal, length());

    job->setQueueCount(job->getQueueCount()+1);
    simtime_t d = simTime() - job->getTimestamp();
    job->setTotalQueueingTime(job->getTotalQueueingTime() + d);
    emit(queueingTimeSignal, d);

    sendJob(job, gateIndex);
}

void PassiveQueue::sendJob(Job *job, int gateIndex)
{
    cGate *out = gate("out", gateIndex);
    EV_INFO << getName() << " sending " << job->getName() << " on gate " << gateIndex << endl;
    send(job, out);
    check_and_cast<IServer *>(out->getPathEndGate()->getOwnerModule())->allocate();
}

/*
 * Returns which of the first N (it's own) resources are occupied
 */
double PassiveQueue::load()
{
    int numLoaded = 0;
    for (int i=0; i<selfServers; i++) {
        cGate *out = gate("out", i);
        IServer *server = check_and_cast<IServer *>(out->getPathEndGate()->getOwnerModule());
        if (!server->isIdle()) {
            numLoaded++;
        }
    }
    return numLoaded / (double)selfServers * 100;
}

double PassiveQueue::power()
{
    double totalPowerConsumed = 0;
    for (int i=0; i<selfServers; i++) {
        cGate *out = gate("out", i);
        IServer *server = check_and_cast<IServer *>(out->getPathEndGate()->getOwnerModule());
//        EV_DEBUG << "server [" << i << "] has consumed " << server->power() << "mA\n";
        totalPowerConsumed += server->power();
    }
    return totalPowerConsumed;
}

int PassiveQueue::bfPossiblyContains(std::string name) {
    const char * c = name.c_str();
    return bf.possiblyContains((uint8_t*)c, strlen(c));
}

void PassiveQueue::bfAdd(std::string name) {
    const char * c = name.c_str();
    bf.add((uint8_t*)c, strlen(c));
}

}; //namespace

