//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2006-2015 OpenSim Ltd.
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "Server.h"
#include "Job.h"
#include "SelectionStrategies.h"
#include "IPassiveQueue.h"

namespace queueing {

Define_Module(Server);

Server::Server()
{
    selectionStrategy = nullptr;
    jobServiced = nullptr;
    endServiceMsg = nullptr;
    allocated = false;
    serviceTime = 0;
    totalPowerConsumed = 0;
}

Server::~Server()
{
    delete selectionStrategy;
    delete jobServiced;
    cancelAndDelete(endServiceMsg);
}

void Server::initialize()
{
    busySignal = registerSignal("busy");
    selectedFromSignal = registerSignal("selectedFrom");
    powerConsumedSignal = registerSignal("powerConsumed");
    emit(busySignal, false);

    endServiceMsg = new cMessage("end-service");
    jobServiced = nullptr;
    allocated = false;
    selectionStrategy = SelectionStrategy::create(par("fetchingAlgorithm"), this, true);
    if (!selectionStrategy)
        throw cRuntimeError("invalid selection strategy");
    serviceTime = 0;
    totalPowerConsumed = 0;
}

void Server::handleMessage(cMessage *msg)
{
    if (msg == endServiceMsg) {
        EV_DEBUG << "End service message: job service time is " << jobServiced->getTotalServiceTime();
        ASSERT(jobServiced != nullptr);
        ASSERT(allocated);
        // add the time that the end service self message was sent to the total service time
        simtime_t d = simTime() - endServiceMsg->getSendingTime();
        jobServiced->setTotalServiceTime(jobServiced->getTotalServiceTime() + d);
        EV_INFO << getName() << " finished servicing " << jobServiced->getName() << " in " << jobServiced->getTotalServiceTime() << " seconds.\n";
        double powerConsumed = serviceTime * jobServiced->getPowerConsumption();
        EV_DEBUG << "power consumed: " << serviceTime << "s * " << jobServiced->getPowerConsumption() << "mA = "<< powerConsumed << endl;
        emit(powerConsumedSignal, powerConsumed);
        totalPowerConsumed += powerConsumed;
        EV_DEBUG << "total power consumed: " << totalPowerConsumed << "mA\n";
        send(jobServiced, "out");
        jobServiced = nullptr;
        allocated = false;
        serviceTime = 0;
        emit(busySignal, false);

        // examine all input queues, and request a new job from a non empty queue
        int k = selectionStrategy->select();
        if (k >= 0) {
            emit(selectedFromSignal, k);
            cGate *gate = selectionStrategy->selectableGate(k);
            EV_INFO << getName() << " requesting job from " << gate->getOwnerModule()->getName() << endl;
            check_and_cast<IPassiveQueue *>(gate->getOwnerModule())->request(gate->getIndex());
        } else {
            EV_INFO << getName() << " - no jobs found\n";
        }
    }
    else {
        if (!allocated)
            error("job arrived, but the sender did not call allocate() previously");
        if (jobServiced)
            throw cRuntimeError("a new job arrived while already servicing one");

        jobServiced = check_and_cast<Job *>(msg);
        EV_INFO << getName() << " received " << jobServiced->getName() << " to service\n";
        serviceTime = par("serviceTime");
        simtime_t simServiceTime = serviceTime;
        scheduleAt(simTime()+simServiceTime, endServiceMsg);
        emit(busySignal, true);
    }
}

void Server::refreshDisplay() const
{
    getDisplayString().setTagArg("i2", 0, jobServiced ? "status/execute" : "");
}

void Server::finish()
{
}

bool Server::isIdle()
{
    return !allocated;  // we are idle if nobody has allocated us for processing
}

void Server::allocate()
{
    allocated = true;
}

double Server::power()
{
    return totalPowerConsumed;
}

}; //namespace

