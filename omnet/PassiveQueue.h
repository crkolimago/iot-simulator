//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2006-2015 OpenSim Ltd.
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifndef __QUEUEING_PASSIVE_QUEUE_H
#define __QUEUEING_PASSIVE_QUEUE_H

#include "QueueingDefs.h"
#include "IPassiveQueue.h"
#include "SelectionStrategies.h"
#include "Job.h"
#include "BloomFilter.h"

namespace queueing {

class SelectionStrategy;

/**
 * A passive queue, designed to co-operate with IServer using method calls.
 */
class QUEUEING_API PassiveQueue : public cSimpleModule, public IPassiveQueue
{
    private:
		simsignal_t droppedSignal;
		simsignal_t queueLengthSignal;
		simsignal_t queueingTimeSignal;

		int selfServers;
        bool fifo;
        int capacity;
        cQueue queue;
        SelectionStrategy *selectionStrategy;
        BloomFilter bf = { 64, 4 };

        void queueLengthChanged();
        void sendJob(Job *job, int gateIndex);

    protected:
        virtual void initialize() override;
        virtual void handleMessage(cMessage *msg) override;
        virtual void refreshDisplay() const override;

    public:
        PassiveQueue();
        virtual ~PassiveQueue();
        virtual Job *peek();
        // The following methods are called from IServer:
        virtual int length() override;
        virtual void request(int gateIndex) override;

        virtual double load();
        virtual double power();

        virtual int bfPossiblyContains(std::string);
        virtual void bfAdd(std::string);
};

}; //namespace

#endif

