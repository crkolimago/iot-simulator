//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2006-2015 OpenSim Ltd.
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include "SelectionStrategies.h"
#include "PassiveQueue.h"
#include "Server.h"

namespace queueing {

SelectionStrategy::SelectionStrategy(cSimpleModule *module, bool selectOnInGate)
{
    hostModule = module;
    isInputGate = selectOnInGate;
    gateSize = isInputGate ? hostModule->gateSize("in") : hostModule->gateSize("out");
}

SelectionStrategy::~SelectionStrategy()
{
}

SelectionStrategy *SelectionStrategy::create(const char *algName, cSimpleModule *module, bool selectOnInGate)
{
    SelectionStrategy *strategy = nullptr;

    if (strcmp(algName, "test") == 0) {
        strategy = new TestSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "priority") == 0) {
        strategy = new PrioritySelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "random") == 0) {
        strategy = new RandomSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "roundRobin") == 0) {
        strategy = new RoundRobinSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "shortestQueue") == 0) {
        strategy = new ShortestQueueSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "longestQueue") == 0) {
        strategy = new LongestQueueSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "jobImportance") == 0) {
            strategy = new JobImportanceSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "bloomFilter") == 0) {
            strategy = new BloomFilterSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "leastLoad") == 0) {
            strategy = new LeastLoadSelectionStrategy(module, selectOnInGate);
    }
    else if (strcmp(algName, "leastPower") == 0) {
            strategy = new LeastPowerConsumedSelectionStrategy(module, selectOnInGate);
    }

    return strategy;
}

cGate *SelectionStrategy::selectableGate(int i)
{
    if (isInputGate)
        return hostModule->gate("in", i)->getPreviousGate();
    else
        return hostModule->gate("out", i)->getNextGate();
}

bool SelectionStrategy::isSelectable(cModule *module)
{
    if (isInputGate) {
        IPassiveQueue *pqueue = dynamic_cast<IPassiveQueue *>(module);
        if (pqueue != nullptr)
            return pqueue->length() > 0;
    }
    else {
        IServer *server = dynamic_cast<IServer *>(module);
        if (server != nullptr)
            return server->isIdle();
    }
    throw cRuntimeError("Only IPassiveQueue (as input) and IServer (as output) is supported by this Strategy");
}

// if true, return 0 because it means the server's "own" queue is available
bool SelectionStrategy::prioritizeOwn()
{
    return isSelectable(selectableGate(0)->getOwnerModule());
}

// --------------------------------------------------------------------------------------------

TestSelectionStrategy::TestSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int TestSelectionStrategy::select()
{
    // return the smallest selectable index
    for (int i = 0; i < gateSize; i++) {
        cModule *ownerModule = selectableGate(i)->getOwnerModule();
        bool canBeSelected = isSelectable(ownerModule);
        IPassiveQueue *queue = check_and_cast<IPassiveQueue *>(ownerModule);
        double load = queue->load();
        EV_DEBUG << hostModule->getName() << " is checking gate " << i << " which belongs to " << ownerModule->getName() << " and is " << load << "% loaded\n";
        if (canBeSelected)
            return i;
    }

    // if none of them is selectable return an invalid no.
    return -1;
}

// --------------------------------------------------------------------------------------------

PrioritySelectionStrategy::PrioritySelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int PrioritySelectionStrategy::select()
{
    // return the smallest selectable index
    for (int i = 0; i < gateSize; i++) {
        cModule *ownerModule = selectableGate(i)->getOwnerModule();
        bool canBeSelected = isSelectable(ownerModule);
        if (canBeSelected)
            return i;
    }

    // if none of them is selectable return an invalid no.
    return -1;
}

// --------------------------------------------------------------------------------------------

RandomSelectionStrategy::RandomSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int RandomSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        return 0;
    }

    // return the smallest selectable index
    int noOfSelectables = 0;

    for (int i = 1; i < gateSize; i++)
        if (isSelectable(selectableGate(i)->getOwnerModule()))
            noOfSelectables++;

    int rnd = hostModule->intuniform(1, noOfSelectables);

    for (int i = 1; i < gateSize; i++)
        if (isSelectable(selectableGate(i)->getOwnerModule()) && (--rnd == 0))
            return i;

//    for (int i = 0; i < gateSize; i++)
//        if (isSelectable(selectableGate(i)->getOwnerModule()))
//            noOfSelectables++;
//
//    int rnd = hostModule->intuniform(1, noOfSelectables);
//
//    for (int i = 0; i < gateSize; i++)
//        if (isSelectable(selectableGate(i)->getOwnerModule()) && (--rnd == 0))
//            return i;

    return -1;
}

// --------------------------------------------------------------------------------------------

RoundRobinSelectionStrategy::RoundRobinSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
    lastIndex = 0;
//    lastIndex = -1;
}

int RoundRobinSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        return 0;
    }

    for (int i = 1; i < gateSize; i++) {
        lastIndex = (lastIndex + 1) % gateSize;
        if (lastIndex == 0) {
            lastIndex++;
        }
        if (isSelectable(selectableGate(lastIndex)->getOwnerModule())) {
            return lastIndex;
        }
    }

    // return the smallest selectable index
//    for (int i = 0; i < gateSize; ++i) {
//        lastIndex = (lastIndex+1) % gateSize;
//        if (isSelectable(selectableGate(lastIndex)->getOwnerModule()))
//            return lastIndex;
//    }

    // if none of them is selectable return an invalid no.
    return -1;
}

// --------------------------------------------------------------------------------------------

ShortestQueueSelectionStrategy::ShortestQueueSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int ShortestQueueSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        return 0;
    }

    // return the smallest selectable index
    int result = -1;  // by default none of them is selectable
    int sizeMin = INT_MAX;
    for (int i = 1; i < gateSize; ++i) {
//    for (int i = 0; i < gateSize; ++i) {
        cModule *module = selectableGate(i)->getOwnerModule();
        int length = (check_and_cast<IPassiveQueue *>(module))->length();
        if (isSelectable(module) && (length < sizeMin)) {
            sizeMin = length;
            result = i;
        }
    }
    return result;
}

// --------------------------------------------------------------------------------------------

LongestQueueSelectionStrategy::LongestQueueSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int LongestQueueSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        return 0;
    }

    // return the longest selectable queue
    int result = -1;  // by default none of them is selectable
    int sizeMax = -1;
//    for (int i = 0; i < gateSize; ++i) {
    for (int i = 1; i < gateSize; ++i) {
        cModule *module = selectableGate(i)->getOwnerModule();
        int length = (check_and_cast<IPassiveQueue *>(module))->length();
        if (isSelectable(module) && length > sizeMax) {
            sizeMax = length;
            result = i;
        }
    }
    return result;
}

// --------------------------------------------------------------------------------------------

JobImportanceSelectionStrategy::JobImportanceSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int JobImportanceSelectionStrategy::select()
{
//    if (prioritizeOwn()) {
//        return 0;
//    }

    EV_INFO << hostModule->getName() << " is selecting a job with strategy jobImportance\n";
    int highestImportance = -1; // note: default is 0 (this converges to priority gate selection if all are default)
    int gate = -1;
    for (int i = 0; i < gateSize; i++) {
//    for (int i = 1; i < gateSize; i++) {
        // if the queue has a job, if the importance is greater than the previous highest importance, select it
        cModule *module = selectableGate(i)->getOwnerModule();
        if (isSelectable(module)) {
            // look at next job in the queue
            Job *front = (check_and_cast<IPassiveQueue *>(module))->peek();
            // compare to previously highest importance
            int importance = front->getImportance();
            EV_DEBUG << module->getName() << " contains a job and its importance is " << importance << ", (prev = " << highestImportance << ")\n";
            if (importance > highestImportance) {
                highestImportance = importance;
                gate = i;
            }
        } else {
            EV_DEBUG << module->getName() << " is not selectable\n";
        }
    }

    return gate;
}

// --------------------------------------------------------------------------------------------

//JobCyclesSelectionStrategy::JobCyclesSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
//    SelectionStrategy(module, selectOnInGate)
//{
//}
//
//int JobCyclesSelectionStrategy::select()
//{
//    EV_INFO << hostModule->getName() << " is selecting a job with strategy jobCycles\n";
//    int minCycles = INT_MAX;
//    int gate = -1;
//    for (int i = 0; i < gateSize; i++) {
//        cModule *module = selectableGate(i)->getOwnerModule();
//        if (isSelectable(module)) {
//            // look at next job in the queue
//            Job *front = (check_and_cast<IPassiveQueue *>(module))->peek();
//            int cycles = front->getCycles();
//            EV_DEBUG << module->getName() << " contains a job and its cycles are " << cycles << ", (prev = " << minCycles << ")\n";
//            if (cycles < minCycles) {
//                minCycles = cycles;
//                gate = i;
//            }
//        } else {
//            EV_DEBUG << module->getName() << " is not selectable\n";
//        }
//    }
//
//    return gate;
//}

// --------------------------------------------------------------------------------------------

//JobPowerConsumptionSelectionStrategy::JobPowerConsumptionSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
//    SelectionStrategy(module, selectOnInGate)
//{
//}
//
//int JobPowerConsumptionSelectionStrategy::select()
//{
//    EV_INFO << hostModule->getName() << " is selecting a job with strategy jobPowerConsumption\n";
//    double minPower = DBL_MAX;
//    int gate = -1;
//    for (int i = 0; i < gateSize; i++) {
//        cModule *module = selectableGate(i)->getOwnerModule();
//        if (isSelectable(module)) {
//            // look at next job in the queue
//            Job *front = (check_and_cast<IPassiveQueue *>(module))->peek();
//            double power = front->getPowerConsumption();
//            EV_DEBUG << module->getName() << " contains a job and its power is " << power << ", (prev = " << minPower << ")\n";
//            if (power < minPower) {
//                minPower = power;
//                gate = i;
//            }
//        } else {
//            EV_DEBUG << module->getName() << " is not selectable\n";
//        }
//    }
//
//    return gate;
//}

// --------------------------------------------------------------------------------------------

//DoNothingSelectionStrategy::DoNothingSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
//    SelectionStrategy(module, selectOnInGate)
//{
//}
//
//int DoNothingSelectionStrategy::select()
//{
//    EV_INFO << hostModule->getName() << " is doing nothing\n";
//    return -1;
//}

// --------------------------------------------------------------------------------------------

BloomFilterSelectionStrategy::BloomFilterSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

// naive bloom filter, if all are contained, then pick the first gate
int BloomFilterSelectionStrategy::select()
{
    EV_INFO << hostModule->getName() << " is selecting a job with strategy bloomFilter\n";
    int gate = -1;
    int minVisited = INT_MAX;

    const char *moduleName = hostModule->getName();

    // poll each prospective gate
    for (int i = 0; i < gateSize; i++) {
        cModule *module = selectableGate(i)->getOwnerModule();
        if (isSelectable(module)) {
            // if selectable, get the bloom potential bloom filter count
            IPassiveQueue *receiver = check_and_cast<IPassiveQueue *>(module);

            int count = receiver->bfPossiblyContains(moduleName);
            const char *receiverModuleName = module->getName();

            // if the count is 0, immediately send to that gate
            if (count == 0) {
                EV_DEBUG << receiverModuleName << " has not received a job from " << moduleName << endl;
                receiver->bfAdd(moduleName);
                return i;
            }

            // otherwise keep a record of the min gate
            if (count < minVisited) {
                EV_DEBUG << receiverModuleName << " has possibly received " << count << " jobs from " << moduleName << endl;
                minVisited = count;
                gate = i;
            }
        } else {
            EV_DEBUG << module->getName() << " is not selectable\n";
        }
    }

    // send to the min and add the sending module's name to the receiver's bloom filter count
    if (gate > -1) {
        IPassiveQueue *queue = check_and_cast<IPassiveQueue *>(selectableGate(gate)->getOwnerModule());
        queue->bfAdd(moduleName);
    }

    return gate;
}

// --------------------------------------------------------------------------------------------

LeastLoadSelectionStrategy::LeastLoadSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int LeastLoadSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        EV_DEBUG << hostModule->getName() << " is selecting its own queue\n";
        return 0;
    }

    // if any of the remaining gates have 0% load, return, otherwise, pick the smallest
    int result = -1;
    bool first = true;
    double loadMax = 0.0;
    for (int i = 1; i < gateSize; ++i) {
        cModule *module = selectableGate(i)->getOwnerModule();
        if (isSelectable(module)) {
            double load = (check_and_cast<IPassiveQueue *>(module))->load();
            EV_DEBUG << hostModule->getName() << " is checking gate " << i << " which belongs to " << module->getName() << " and is " << load << "% loaded\n";
            if (load == 100.0) {
                return i;
            } else if (first) {
                first = false;
                result = i;
                loadMax = load;
            } else if (load > loadMax) {
                loadMax = load;
                result = i;
            }
        }
    }
    return result;
}

// --------------------------------------------------------------------------------------------

// each server keeps track of total power consumption
// at any given time, the queue can query for how much power has been used
LeastPowerConsumedSelectionStrategy::LeastPowerConsumedSelectionStrategy(cSimpleModule *module, bool selectOnInGate) :
    SelectionStrategy(module, selectOnInGate)
{
}

int LeastPowerConsumedSelectionStrategy::select()
{
    if (prioritizeOwn()) {
        EV_DEBUG << hostModule->getName() << " is selecting its own queue\n";
        return 0;
    }

    // look for the node which has spent the most power (least battery remaining)
    int result = -1;
    bool first = true;
    double maxPower = 0.0;
    for (int i = 1; i < gateSize; ++i) {
        cModule *module = selectableGate(i)->getOwnerModule();
        if (isSelectable(module)) {
            double power = (check_and_cast<IPassiveQueue *>(module))->power();
            EV_DEBUG << hostModule->getName() << " is checking gate " << i << " which belongs to " << module->getName() << " and has consumed " << power << "mA\n";
            if (power == 0) {
                return i;
            } else if (first) {
                first = false;
                result = i;
                maxPower = power;
            } else if (power > maxPower) {
                maxPower = power;
                result = i;
            }
        }
    }
    return result;
}

// --------------------------------------------------------------------------------------------

}; //namespace

